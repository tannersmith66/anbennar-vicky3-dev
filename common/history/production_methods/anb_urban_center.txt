﻿# Anbennar - Removed Vanilla tag references + added our own
PRODUCTION_METHODS = {
	c:A01 = { # Anbennar
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
		#All Anb Subjects in the empire
		every_subject_or_below = {
			limit = {
				is_subject_type = puppet
			}
			activate_production_method = {
				building_type = building_urban_center
				production_method = pm_market_squares
			}
			activate_production_method = {
				building_type = building_urban_center
				production_method = pm_gas_streetlights
			}
		}
	}
	c:A03 = { # Lorent
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:A04 = { # Gawed
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:A06 = { # Gnomish Hiearchy
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:A07 = { # Reveria
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:A18 = { # Bayvek
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
	c:A19 = { # Vertesk
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_market_squares
		}
		activate_production_method = {
			building_type = building_urban_center
			production_method = pm_gas_streetlights
		}
	}
}
