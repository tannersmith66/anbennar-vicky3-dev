﻿POPS = {
	s:STATE_AVHAVUBHIYA = {
		region_state:R01 = {
			create_pop = {
				culture = east_damerian
				size = 2000000
			}

		}
	}
	s:STATE_IYARHASHAR = {
		region_state:R01 = {
			create_pop = {
				culture = east_damerian
				size = 1000000
			}

		}
	}
	s:STATE_SOUTH_GHANKEDHEN = {
		region_state:R02 = {
			create_pop = {
				culture = east_damerian
				size = 2000000
			}

		}
	}
	s:STATE_NORTH_GHANKEDHEN = {
		region_state:R02 = {
			create_pop = {
				culture = east_damerian
				size = 2000000
			}

		}
	}
	s:STATE_TUDHINA = {
		region_state:R03 = {
			create_pop = {
				culture = east_damerian
				size = 500000
			}

		}
	}
	s:STATE_PASIRAGHA = {
		region_state:R01 = {
			create_pop = {
				culture = east_damerian
				size = 8000000
			}

		}
	}
	s:STATE_UPPER_DHENBASANA = {
		region_state:R01 = {
			create_pop = {
				culture = east_damerian
				size = 12000000
			}

		}
	}
	s:STATE_LOWER_DHENBASANA = {
		region_state:R01 = {
			create_pop = {
				culture = east_damerian
				size = 12000000
			}

		}
	}
	s:STATE_ASCENSION_JUNGLE = {
		region_state:R04 = {
			create_pop = {
				culture = east_damerian
				size = 1000000
			}

		}
	}
	s:STATE_TUJGAL = {
		region_state:R04 = {
			create_pop = {
				culture = east_damerian
				size = 500000
			}

		}
	}
	s:STATE_BABHAGAMA = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 500000
			}

		}
	}
	s:STATE_SATARSAYA = {
		region_state:R01 = {
			create_pop = {
				culture = east_damerian
				size = 2000000
			}

		}
	}
	s:STATE_WEST_GHAVAANAJ = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 1000000
			}

		}
	}
	s:STATE_EAST_GHAVAANAJ = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 2000000
			}

		}
	}
	s:STATE_TUGHAYASA = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 4000000
			}

		}
	}
	s:STATE_DHUJAT = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 12000000
			}

		}
	}
	s:STATE_SARISUNG = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 12000000
			}

		}
	}
	s:STATE_TILTAGHAR = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 500000
			}

		}
	}
	s:STATE_WEST_NADIMRAJ = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 2000000
			}

		}
	}
	s:STATE_RAJNADHAGA = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 6000000
			}

		}
	}
	s:STATE_CENTRAL_NADIMRAJ = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 5000000
			}

		}
	}
	s:STATE_EAST_NADIMRAJ = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 6000000
			}

		}
	}
	s:STATE_HOBGOBLIN_HOMELANDS = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 3000000
			}

		}
	}
	s:STATE_GHILAKHAD = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 1000000
			}

		}
	}
	s:STATE_RAGHAJANDI = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 5000000
			}

		}
	}
	s:STATE_GHATASAK = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 2000000
			}

		}
	}
	s:STATE_SHAMAKHAD_PLAINS = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 2000000
			}

		}
	}
	s:STATE_SIR = {
		region_state:R72 = {
			create_pop = {
				culture = east_damerian
				size = 4000000
			}

		}
	}
	s:STATE_SRAMAYA = {
		region_state:R05 = {
			create_pop = {
				culture = east_damerian
				size = 10000000
			}

		}
	}
}