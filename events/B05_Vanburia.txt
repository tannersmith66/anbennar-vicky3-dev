﻿namespace = vanburia

vanburia.1 = {
	type = country_event
	placement = ROOT
	
	title = vanburia.1.t
	desc = vanburia.1.d
	flavor = vanburia.1.f
	
	event_image = {
		video = "gfx/event_pictures/europenorthamerica_capitalists_meeting.bk2"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"
	on_opened_soundeffect = "event:/SFX/Events/europenorthamerica/capitalists_meeting"

	icon = "gfx/interface/icons/event_icons/event_portrait.dds"
	
	trigger = {
	}

	immediate = {
		add_journal_entry = {
			type = je_end_of_the_guild
		}
	}

	option = {
		name = vanburia.1.a
		
		show_as_tooltip = {
			add_modifier = {
				name = b05_guild_structure
				months = -1
			}		
		}
	}
}

vanburia.2 = {
	type = country_event
	placement = ROOT
	
	title = vanburia.2.t
	desc = {
		first_valid = {
			triggered_desc = { #Monarchy, no expanded vote
				desc = vanburia.2.d1
				trigger = {
					has_law = law_type:law_monarchy
				}
			}
			triggered_desc = { #Theocracy, no expanded vote
				desc = vanburia.2.d2
				trigger = {
					has_law = law_type:law_theocracy
				}
			}
			triggered_desc = { #Parliamentary or Council Republic somehow, no expanded vote
				desc = vanburia.2.d3
				trigger = {
					OR = {
						has_law = law_type:autocracy
						has_law	= law_type:oligarchy
					}
				}
			}
			triggered_desc = { #Expanded vote as Presidential Republic
				desc = vanburia.2.d4
				trigger = {
					always = yes
				}
			}
		}
	}
	flavor = vanburia.2.f
	
	event_image = {
		video = "gfx/event_pictures/europenorthamerica_capitalists_meeting.bk2"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"
	on_opened_soundeffect = "event:/SFX/Events/europenorthamerica/capitalists_meeting"

	icon = "gfx/interface/icons/event_icons/event_portrait.dds"
	
	duration = 3
	
	trigger = {
		# triggered by je_end_of_the_guild completion
	}

	immediate = {
		ruler = {
			save_scope_as = monarch_scope
		}
	}

	option = { # default for any - ? modifier
		name = vanburia.2.a
		default_option = yes
		
	}

	option = { # monarchy/theocracy option - maybe authority modifier
		name = vanburia.2.b
		trigger = {
			OR = {
				has_law = law_type:law_monarchy
				has_law = law_type:law_theocracy
			}
		}
	}

	option = { # republic option - ? modifier
		name = vanburia.2.c
		trigger = {
			OR = {
				has_law = law_type:law_presidential_republic
				has_law = law_type:law_parliamentary_republic
				has_law = law_type:law_council_republic
			}
		}
		
	}
}
