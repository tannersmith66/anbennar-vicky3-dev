﻿COUNTRIES = {
	c:B23 = {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_wealth_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats		
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_mercantilism # Free trade also possible
		activate_law = law_type:law_per_capita_based_taxation
		# No colonial affairs
		activate_law = law_type:law_local_police
		# No schools - their ideas say it's a university
		activate_law = law_type:law_no_health_system
		activate_law = law_type:law_traditional_magic_encouraged
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		# No migration controls
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_non_monstrous_only

		ig:ig_devout = {
			add_ruling_interest_group = yes
			set_interest_group_name = ig_ravelian_church
			remove_ideology = ideology_moralist
			add_ideology = ideology_ravelian_moralist
			remove_ideology = ideology_pious
			add_ideology = ideology_scholarly
		}
	}
}