﻿DIPLOMACY = {
	c:A30 = {	#Magocratic Demesne-Wyvernheart customs union as Wyvernheart was helped a lot by them
		create_diplomatic_pact = {
			country = c:A26
			type = customs_union
		}
	}

	c:A02 = {	#Vivin
		create_diplomatic_pact = {	#Ravelian is under stinky Vivin CU. They definitely would rather be in Anbennar's as its cooler and has more stuff
			country = c:A33
			type = customs_union
		}
	}

	c:B19 = {	#Trollsbay Federation, lead by Cestirmark
		create_diplomatic_pact = {
			country = c:B20 #Marlliande
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:B21 #Ynnsmouth
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:B22 #Zanlib
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:B23 #Isobelin
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:B24 #Thilvis
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:B25 #Valorpoint
			type = customs_union
		}
	}
}