﻿POPS = {
	s:STATE_ROILSARD = {
		region_state:A03 = {
			create_pop = {
				culture = roilsardi
				size = 3047301
				split_religion = {
					roilsardi = {
						ravelian = 0.2
						corinite = 0.8
					}
				}
			}
			create_pop = {
				culture = moon_elven
				size = 10231
			}
		}
	}

	s:STATE_ROSECROWN = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 1905032
				split_religion = {
					lorentish = {
						ravelian = 0.2
						regent_court = 0.8
					}
				}
			}
			create_pop = {
				culture = redfoot_halfling
				size = 555100
			}
			create_pop = {
				culture = moon_elven
				size = 27704
			}
			create_pop = {
				culture = ruby_dwarf
				size = 161003
			}
			create_pop = {
				culture = creek_gnome
				size = 7052
			}
		}
	}

	s:STATE_BLOODWINE = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 2003105
			}
			create_pop = {
				culture = redfoot_halfling
				size = 304100
			}
		}
	}

	s:STATE_GREAT_ORDING = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 1105001
			}
		}
	}

	s:STATE_WINEBAY = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 1100401
				split_religion = {
					lorentish = {
						ravelian = 0.3	#growing
						regent_court = 0.7
					}
				}
			}
			create_pop = {
				culture = moon_elven
				size = 6528
			}
			create_pop = {
				culture = redfoot_halfling
				size = 405104
			}
			create_pop = {
				culture = ruby_dwarf
				size = 28050
			}
		}
	}

	s:STATE_SORNCOST = {
		region_state:A03 = {
			create_pop = {
				culture = sorncosti
				size = 1390456
			}
			create_pop = {
				culture = moon_elven
				size = 2040
			}
		}
	}


	s:STATE_VENAIL = {
		region_state:A03 = {
			create_pop = {
				culture = sorncosti
				size = 304982
			}
			create_pop = {
				culture = moon_elven
				size = 6040
			}
		}
	}

	s:STATE_RUBYHOLD = {
		region_state:A75 = {
			create_pop = {
				culture = ruby_dwarf
				size = 2659403
			}
		}
	}

	s:STATE_DAROM = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 1040230
			}
			create_pop = {
				culture = derannic
				size = 205155
			}
		}
	}

	s:STATE_DERANNE = {
		region_state:A03 = {
			create_pop = {
				culture = derannic
				size = 2104042
				split_religion = {
					derannic = {
						corinite = 0.5
						regent_court = 0.5
					}
				}
			}
			create_pop = {
				culture = moon_elven	#Aelvar elves
				size = 6115
			}
			create_pop = {
				culture = creek_gnome
				size = 19104
			}
		}
	}

	s:STATE_REDGLADES = {
		region_state:A65 = {
			create_pop = {
				culture = moon_elven
				size = 453001
			}
		}
	}

	s:STATE_SOUTHROY = {
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 606115
			}
			create_pop = {
				culture = redfoot_halfling
				size = 459100
			}
		}
	}


	#Small Country
	s:STATE_ROYSFORT = {
		region_state:A14 = {
			create_pop = {
				culture = redfoot_halfling
				size = 1203981
			}
			create_pop = {
				culture = creek_gnome
				size = 6005
			}
		}
	}
	s:STATE_THOMSBRIDGE = {
		region_state:A14 = {
			create_pop = {
				culture = redfoot_halfling
				size = 1333186
				split_religion = {
					redfoot_halfling = {
						corinite = 0.25
						regent_court = 0.75
					}
				}
			}
		}
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 310401
			}
			create_pop = {
				culture = redfoot_halfling
				size = 502104
				religion = regent_court
			}
		}
	}
	s:STATE_CIDERFIELD = {
		region_state:A14 = {
			create_pop = {
				culture = bluefoot_halfling
				size = 1093041
				split_religion = {
					bluefoot_halfling = {
						corinite = 0.75
						regent_court = 0.25
					}
				}
			}
			create_pop = {
				culture = redfoot_halfling
				size = 1140205
				split_religion = {
					redfoot_halfling = {
						corinite = 0.75
						regent_court = 0.25
					}
				}
			}
			create_pop = {
				culture = creek_gnome
				size = 2101
			}
		}
	}
	s:STATE_HOMEHILLS = {
		region_state:A14 = {
			create_pop = {
				culture = redfoot_halfling
				size = 1802010
				split_religion = {
					redfoot_halfling = {
						ravelian = 0.25
						regent_court = 0.25
						corinite = 0.5
					}
				}
			}
			create_pop = {
				culture = bluefoot_halfling
				size = 1444122
				split_religion = {
					bluefoot_halfling = {
						ravelian = 0.5
						corinite = 0.5
					}
				}
			}
			create_pop = {
				culture = creek_gnome
				size = 65020
			}
		}
	}
	s:STATE_ELKMARCH = {
		region_state:A14 = {
			create_pop = {
				culture = bluefoot_halfling
				size = 1203211
				split_religion = {
					bluefoot_halfling = {
						ravelian = 0.5
						corinite = 0.5
					}
				}
			}
		}
	}
	s:STATE_BEEPECK = {
		region_state:A14 = {
			create_pop = {
				culture = beefoot_halfling
				size = 1303211
			}
			create_pop = {
				culture = west_damerian
				size = 105002
			}
		}
	}
	s:STATE_DRAGONHILLS = {
		region_state:A14 = {
			create_pop = {
				culture = bluefoot_halfling
				size = 246030
				split_religion = {
					bluefoot_halfling = {
						ravelian = 0.5
						corinite = 0.5
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 2764
			}
		}
	}
}